---
author: Kevin R, Coombes
date: 17 October 2018
title: Organizer/Doc/Paper
---

# Overview
This folder contains the source material of an article describing the
research project and intended for publication.
