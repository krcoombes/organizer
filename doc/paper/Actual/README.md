---
author: Kevin R. Coombes
title: Manuscript
date: 2017-12-14
---

# Overview
This folder contains draft(s) of the "WHATEVER" manuscript.

1. `results.tex` is the primary LaTeX source for the results
   section. In some cases, this will be produced from an Sweave file
   called (cleverly enough) `results.Rnw`.
1. `discussion.tex` is the primary LaTeX source for the discussion
   section.
1. `introduction.tex` is the primary source for the introduction section.
1. `wrapper.tex` is a master file that can be used to include the
   other parts into a single document.  To produce the full PDF file,
   you must first run the command `Sweave("results.Rnw")` in an R
   session to produce the LaTeX version of the results section. You
   then run the coimmand `pdflatex wrapper` in a command window to
   produce the full `wrapper.pdf` file.

# File Conversions
As you can tell from the list above, we are planning to use LaTeX as
the primary source. For those who prefer to edit woth Word, the
recommended procedure is

1. Pull the latest LaTeX version of the file from the repository.
2. Use a command like `pandoc -o FILE.docx FILE.tex` to convert
   the LaTeX source to Word.
3. Make your edits.
4. Use `pandoc -o FILE.tex FILE.docx` to covert back.
5. Push or create a merge request using the new LaTeX file.

I'm not sure how this will work with fancy features like comments or
track changes in Word. I suspect we want to avoid track changes (since
the version control will do that for us). Comments might be nice if we
can figure out how to get them to work.
