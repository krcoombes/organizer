---
author: Kevin R, Coombes
date: 17 October 2018
title: Organizer/Doc/Presentation
---

# Overview
This folder contains the source material for a talk about the
research. In some cases, it may also hold the source material for a
poster presenting the research at a conference.
