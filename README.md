---
author: Kevin R, Coombes
date: 17 October 2018
title: Organizing for Reproducibility and Collaboration
---

# Overview

# How to use

+ Create a new empty [Git] project. (You can, of course, use a
  different version control system (VCS).)
+ Clone that VCS project to a folder on your local machine.
+ Download the [latest release] of this repository, and unzip it into
  the folder you just created.
+ Update the various "README.md" files to customize them to your
  project.
+ Add all the files to the project, and commit it to your VCS.


[Git]: https://git-scm.com/
[latest release]: Organizer-1.0.tar.gz
