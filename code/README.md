---
author: Kevin R. Coombes
date: 24 October 2018
title: Organizer/Code
---

# Overview
This folder contains the computer code used to perform all analyses
related to the project.
